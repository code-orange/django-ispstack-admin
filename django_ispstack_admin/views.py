import requests
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.translation import gettext as _

from django_ispstack_main.django_ispstack_main.views import get_nav
from django_voipstack_mdat.django_voipstack_mdat.models import *


def get_form_data_from_voip_api(action_id: int, request=None):
    r = requests.get(
        settings.VOIPSTACK_API_URL + "/v1/mandatoryattrs/" + str(action_id),
        headers={
            "Authorization": "ApiKey "
            + settings.VOIPSTACK_API_USER
            + ":"
            + settings.VOIPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return_data = r.json()
    form_data = return_data[str(action_id)]

    for form_attribute in form_data:
        form_attribute["translation"] = _(form_attribute["attribute"])

    if request is not None:
        for key, value in request.GET.items():
            for form_attribute in form_data:
                if form_attribute["attribute"] == key:
                    form_attribute["settings"]["default_value"] = value

    form_data = sorted(form_data, key=lambda i: i["settings"]["form_order"])

    return form_data


def get_form_data_from_isp_api(action_id: str, request=None):
    r = requests.get(
        settings.ISPSTACK_API_URL + "/v1/mandatoryattrs/" + action_id,
        headers={
            "Authorization": "ApiKey "
            + settings.ISPSTACK_API_USER
            + ":"
            + settings.ISPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return_data = r.json()
    form_data = return_data[action_id]

    for form_attribute in form_data:
        form_attribute["translation"] = _(form_attribute["attribute"])

    if request is not None:
        for key, value in request.GET.items():
            for form_attribute in form_data:
                if form_attribute["attribute"] == key:
                    form_attribute["settings"]["default_value"] = value

    form_data = sorted(form_data, key=lambda i: i["settings"]["form_order"])

    return form_data


def forward_form_data_to_voip_api(request):
    post_data = request.POST.dict()

    if "csrfmiddlewaretoken" in post_data:
        del post_data["csrfmiddlewaretoken"]

    r = requests.post(
        settings.VOIPSTACK_API_URL + "/v1/job",
        headers={
            "Authorization": "ApiKey "
            + settings.VOIPSTACK_API_USER
            + ":"
            + settings.VOIPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=post_data,
    )
    return HttpResponseRedirect("/dashboard")


def forward_form_data_to_isp_api(request):
    post_data = request.POST.dict()

    if "csrfmiddlewaretoken" in post_data:
        del post_data["csrfmiddlewaretoken"]

    r = requests.post(
        settings.ISPSTACK_API_URL + "/v1/cpe",
        headers={
            "Authorization": "ApiKey "
            + settings.ISPSTACK_API_USER
            + ":"
            + settings.ISPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=post_data,
    )
    return HttpResponseRedirect("/dashboard")


def list_trunks(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/list_trunks.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Trunks")

    template_opts["menu_groups"] = get_nav()

    r = requests.get(
        settings.VOIPSTACK_API_URL + "/v1/trunk",
        headers={
            "Authorization": "ApiKey "
            + settings.VOIPSTACK_API_USER
            + ":"
            + settings.VOIPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    api_data = r.json()
    all_trunks = api_data["objects"]

    template_opts["all_trunks"] = all_trunks

    return HttpResponse(template.render(template_opts, request))


def add_trunk(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/add_trunk.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Add Trunk")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(1, request)
    template_opts["form_action"] = "/telephony/post_add_trunk"

    return HttpResponse(template.render(template_opts, request))


def list_numbers(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/list_numbers.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Numbers")

    template_opts["menu_groups"] = get_nav()

    template_opts["all_numbers"] = VoipMdatNumbers.objects.all()

    return HttpResponse(template.render(template_opts, request))


def add_number(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Add Number")
    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(3, request)
    template_opts["form_action"] = "/telephony/post_add_number"

    template = loader.get_template("django_ispstack_admin/add_number.html")

    return HttpResponse(template.render(template_opts, request))


def add_block_number(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Add Block Number")
    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(4, request)
    template_opts["form_action"] = "/telephony/post_add_block_number"

    template = loader.get_template("django_ispstack_admin/add_block_number.html")

    return HttpResponse(template.render(template_opts, request))


def del_trunk(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/add_trunk.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete Trunk")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(2, request)
    template_opts["form_action"] = "/telephony/post_del_trunk"

    return HttpResponse(template.render(template_opts, request))


def port_number(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/port_number.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Port Number")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(5, request)
    template_opts["form_action"] = "/telephony/post_port_number"

    return HttpResponse(template.render(template_opts, request))


def port_block_number(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/port_block_number.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Port Block Number")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(6, request)
    template_opts["form_action"] = "/telephony/post_port_block_number"

    return HttpResponse(template.render(template_opts, request))


def change_master_data(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/change_master_data.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Change Master Data")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(7, request)
    template_opts["form_action"] = "/telephony/post_change_master_data"

    return HttpResponse(template.render(template_opts, request))


def cancel_transaction(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/cancel_transaction.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Cancel Transaction")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(8, request)
    template_opts["form_action"] = "/telephony/post_cancel_transaction"

    return HttpResponse(template.render(template_opts, request))


def cancel_number(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/cancel_number.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Cancel Number")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(9, request)
    template_opts["form_action"] = "/telephony/post_cancel_number"

    return HttpResponse(template.render(template_opts, request))


def publish_wbci_outgoing(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/publish_wbci_outgoing.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Publish WBCI Outgoing")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(10, request)
    template_opts["form_action"] = "/telephony/post_publish_wbci_outgoing"

    return HttpResponse(template.render(template_opts, request))


def shift_wbci_outgoing(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/shift_wbci_outgoing.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Shift WBCI Outgoing")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(11, request)
    template_opts["form_action"] = "/telephony/post_shift_wbci_outgoing"

    return HttpResponse(template.render(template_opts, request))


def del_wbci_outgoing(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/del_wbci_outgoing.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete WBCI Outgoing")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(12, request)
    template_opts["form_action"] = "/telephony/post_del_wbci_outgoing"

    return HttpResponse(template.render(template_opts, request))


def get_wbci_info(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/get_wbci_info.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Get WBCI Contract Information")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_voip_api(13, request)
    template_opts["form_action"] = "/telephony/post_get_wbci_info"

    return HttpResponse(template.render(template_opts, request))


def list_cpe(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/list_cpe.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Customer Premises Equipment (CPE)")

    template_opts["menu_groups"] = get_nav()

    r = requests.get(
        settings.ISPSTACK_API_URL + "/v1/cpe",
        headers={
            "Authorization": "ApiKey "
            + settings.ISPSTACK_API_USER
            + ":"
            + settings.ISPSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return_data = r.json()
    all_cpe = return_data["objects"]

    template_opts["all_cpe"] = all_cpe

    return HttpResponse(template.render(template_opts, request))


def add_cpe(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/add_cpe.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Add Customer Premises Equipment (CPE)")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_isp_api("cpe", request)
    template_opts["form_action"] = "/access/post_add_cpe"

    return HttpResponse(template.render(template_opts, request))


def edit_cpe(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/edit_cpe.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Edit Customer Premises Equipment (CPE)")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_isp_api("cpe", request)
    template_opts["form_action"] = "/access/post_edit_cpe"

    return HttpResponse(template.render(template_opts, request))


def edit_cpe_auth(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/edit_cpe_auth.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Edit Authentification for CPE")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_isp_api("cpe", request)
    template_opts["form_action"] = "/access/post_edit_cpe_auth"

    return HttpResponse(template.render(template_opts, request))


def cancel_cpe(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_admin/cancel_cpe.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Cancel Customer Premises Equipment (CPE)")

    template_opts["menu_groups"] = get_nav()

    template_opts["form_data"] = get_form_data_from_isp_api("cpe", request)
    template_opts["form_action"] = "/access/post_cancel_cpe"

    return HttpResponse(template.render(template_opts, request))
